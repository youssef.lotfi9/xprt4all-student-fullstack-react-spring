package xperts4all.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xperts4all.demo.dto.StudentDto;
import xperts4all.demo.entities.Student;
import xperts4all.demo.services.StudentServiceImpl;

import java.util.UUID;

@RestController
@RequestMapping("/students")
public class StudentController {

    private final StudentServiceImpl service;

    @Autowired
    public StudentController(StudentServiceImpl service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Student> getStudentById(@PathVariable UUID id) {
        return new ResponseEntity<>(service.findStudentById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> saveStudent(@RequestBody StudentDto studentDto) {
        return new ResponseEntity<>(service.saveStudent(studentDto), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteStudentById(@PathVariable UUID id) {
        service.deleteStudentById(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    public ResponseEntity<?> getAllStudent() {
        return new ResponseEntity<>(service.findAllStudents(), HttpStatus.OK);
    }
//    @PutMapping("/{id}")
//    public ResponseEntity<?> updateStudent(@RequestBody Student student, @PathVariable UUID id){
//        var updatedStudent = service.updateStudent(student);
//
//    }
}
