package xperts4all.demo.controller;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xperts4all.demo.dto.UserReq;
import xperts4all.demo.dto.UserRes;

@RestController
@RequestMapping("/users")
public class UserController {

    private final AuthenticationProvider authenticationProvider;

    public UserController(AuthenticationProvider authenticationProvider) {
        this.authenticationProvider = authenticationProvider;
    }

    @PostMapping
    public UserRes authenticate(@RequestBody UserReq requestDto) {
        Authentication authentication = new UsernamePasswordAuthenticationToken(requestDto.getUserName(), requestDto.getPassword());
        UserRes responseDto = new UserRes();
        try {
            Authentication authenticationResult = authenticationProvider.authenticate(authentication);
            responseDto.setCode("200");
            responseDto.setMessage("Success authentication");
            responseDto.setRole("" + authenticationResult.getAuthorities());
            SecurityContext sc = SecurityContextHolder.getContext();
            sc.setAuthentication(authenticationResult);
        } catch (AuthenticationException e) {
            responseDto.setCode("401");
            responseDto.setMessage("fail authentication");
        }
        return responseDto;
    }
}
