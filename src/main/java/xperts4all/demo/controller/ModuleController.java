package xperts4all.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xperts4all.demo.dto.ModuleDto;
import xperts4all.demo.services.ModuleServiceImpl;

import java.util.UUID;

@RestController
@RequestMapping("/students/{idS}/modules")
public class ModuleController {

    private final ModuleServiceImpl service;

    @Autowired
    public ModuleController(ModuleServiceImpl service) {
        this.service = service;
    }

    @GetMapping("/{idM}")
    public ResponseEntity<?> getModuleById(@PathVariable("idS") String idStudent, @PathVariable("idM") String idModule) {
        return new ResponseEntity<>(service.findModuleById(UUID.fromString(idStudent), UUID.fromString(idModule)), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> getAllModule(@PathVariable("idS") String idStudent) {
        return new ResponseEntity<>(service.findAllModules(UUID.fromString(idStudent)), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> saveModule(@PathVariable("idS") String idStudent, @RequestBody ModuleDto moduleDto) {
        moduleDto.setId(UUID.randomUUID());
        return new ResponseEntity<>(service.saveModule(UUID.fromString(idStudent), moduleDto), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteModuleById(@PathVariable("idS") String idStudent, @PathVariable("id") String id) {
        service.deleteModuleById(UUID.fromString(idStudent), UUID.fromString(id));
        return ResponseEntity.ok().build();
    }
}
