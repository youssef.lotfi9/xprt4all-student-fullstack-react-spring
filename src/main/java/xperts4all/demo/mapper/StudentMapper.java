package xperts4all.demo.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import xperts4all.demo.dto.StudentDto;
import xperts4all.demo.entities.Student;

import java.util.List;

@Mapper(componentModel = "spring")
public interface StudentMapper {
    Student studentToEntity(StudentDto studentDto);

    StudentDto studentToDto(Student student);

    List<Student> studentToEntity(List<StudentDto> studentDtos);

    List<StudentDto> studentToDto(List<Student> students);
}
