package xperts4all.demo.mapper;

import org.mapstruct.Mapper;
import xperts4all.demo.dto.ModuleDto;
import xperts4all.demo.entities.Module;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ModuleMapper {
    Module moduleToEntity(ModuleDto moduleDto);

    ModuleDto moduleToDto(Module module);

    List<Module> moduleToEntity(List<ModuleDto> moduleDtos);

    List<ModuleDto> moduleToDto(List<Module> modules);
}
