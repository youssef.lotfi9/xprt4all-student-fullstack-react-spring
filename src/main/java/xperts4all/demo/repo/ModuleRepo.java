package xperts4all.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import xperts4all.demo.entities.Module;

import java.util.List;
import java.util.UUID;

@Repository
public interface ModuleRepo extends JpaRepository<Module, UUID> {
    @Query(value = "select m.* from student s, module m where s.id = m.id_student_fk and s.id = :id ",
            nativeQuery = true)
    List<Module> findAllByStudentId(@Param("id") UUID idStudent);

    @Query(nativeQuery = true, value = "select m.* from student s, module m where s.id = m.id_student_fk and s.id = :id and m.id = :idModule")
    Module findModuleByStudentId(@Param("id") UUID idStudent, @Param("idModule") UUID idModule);

    @Query(value = "delete from module m where m.id = :id and m.id_student_fk = :idModule", nativeQuery = true)
    void deleteModuleByStudentId(@Param("id") UUID studentId, @Param("idModule") UUID moduleId);

}
