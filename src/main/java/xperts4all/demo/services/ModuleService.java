package xperts4all.demo.services;

import xperts4all.demo.dto.ModuleDto;
import xperts4all.demo.entities.Module;

import java.util.List;
import java.util.UUID;

public interface ModuleService {

    Module findModuleById(UUID idStudent, UUID idModule);
    List<Module> findAllModules(UUID id);
    void deleteModuleById(UUID idStudent , UUID idModule);
    Module saveModule(UUID idStudent, ModuleDto module);
    Module updateModule(UUID idStudent , Module module);

}
