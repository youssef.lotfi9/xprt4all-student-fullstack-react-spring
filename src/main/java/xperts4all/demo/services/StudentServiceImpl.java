package xperts4all.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xperts4all.demo.dto.StudentDto;
import xperts4all.demo.entities.Student;
import xperts4all.demo.mapper.StudentMapper;
import xperts4all.demo.repo.StudentRepo;

import java.util.List;
import java.util.UUID;

@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepo repository;
    private final StudentMapper mapper;

    @Autowired
    public StudentServiceImpl(StudentRepo repository, StudentMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public Student findStudentById(UUID id) {
        return repository.findById(id).orElseThrow();
    }

    @Override
    public List<Student> findAllStudents() {
        return repository.findAll();
    }

    @Override
    public void deleteStudentById(UUID id) {
        repository.deleteById(id);
    }

    @Override
    public Student saveStudent(StudentDto studentDto) {
        studentDto.setId(UUID.randomUUID());
        if (studentDto.getModules().size() != 0)
            studentDto.getModules().forEach(module -> module.setId(UUID.randomUUID()));
        var xz = mapper.studentToEntity(studentDto);
        return repository.save(xz);
    }

    @Override
    public Student updateStudent(StudentDto student) {
        return repository.save(mapper.studentToEntity(student));
    }
}
