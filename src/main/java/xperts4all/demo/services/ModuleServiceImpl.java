package xperts4all.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xperts4all.demo.dto.ModuleDto;
import xperts4all.demo.entities.Module;
import xperts4all.demo.mapper.ModuleMapper;
import xperts4all.demo.mapper.StudentMapper;
import xperts4all.demo.repo.ModuleRepo;

import java.util.List;
import java.util.UUID;

@Service
public class ModuleServiceImpl implements ModuleService {

    private final ModuleRepo repository;

    private final StudentServiceImpl studentService;
    private final ModuleMapper mapper;

    private final StudentMapper studentMapper;

    @Autowired
    public ModuleServiceImpl(ModuleRepo repository, StudentServiceImpl studentService, ModuleMapper mapper, StudentMapper studentMapper) {
        this.repository = repository;
        this.studentService = studentService;
        this.mapper = mapper;
        this.studentMapper = studentMapper;
    }

    @Override
    public Module findModuleById(UUID idStudent, UUID idModule) {
        return repository.findModuleByStudentId(idStudent, idModule);
    }

    @Override
    public List<Module> findAllModules(UUID id) {
        return repository.findAllByStudentId(id);
    }

    @Override
    public void deleteModuleById(UUID idStudent, UUID idModule) {
        repository.deleteModuleByStudentId(idStudent, idModule);
    }

    @Override
    public Module saveModule(UUID idStudent, ModuleDto module) {
        var student = studentService.findStudentById(idStudent);
        var oldModules = student.getModules();
        oldModules.add(mapper.moduleToEntity(module));
        student.setModules(oldModules);
        studentService.saveStudent(studentMapper.studentToDto(student));
        return mapper.moduleToEntity(module);
    }

    @Override
    public Module updateModule(UUID idStudent, Module module) {
        return repository.save(module);
    }
}
