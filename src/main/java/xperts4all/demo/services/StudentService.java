package xperts4all.demo.services;

import org.springframework.stereotype.Service;
import xperts4all.demo.dto.StudentDto;
import xperts4all.demo.entities.Student;
import xperts4all.demo.repo.StudentRepo;

import java.util.List;
import java.util.UUID;

public interface StudentService  {
    Student findStudentById(UUID id);
    List<Student> findAllStudents();
    void deleteStudentById(UUID id);
    Student saveStudent(StudentDto student);
    Student updateStudent(StudentDto student);
}
