package xperts4all.demo.entities;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Student {
    @Id
    private UUID id;

    @Column
    private String fullName;

    //    @Column
    @Enumerated(EnumType.STRING)
    private Branch branch;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_student_fk")
    private List<Module> modules;



    /*
     * todo :
     * MERGE : create new children but when delete parent they stay
     * PERSIST : can't create new children , when delete parent , children stay
     * ALL : create new children , when delete it deletes everything
     * */
}
