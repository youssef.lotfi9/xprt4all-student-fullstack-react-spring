package xperts4all.demo.dto;

import lombok.Data;

@Data
public class UserRes {
    private String code;
    private String message;
    private String role;
}
