package xperts4all.demo.dto;

import lombok.*;
import xperts4all.demo.entities.Branch;
import xperts4all.demo.entities.Module;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StudentDto {
    private UUID id;

    private String fullName;

    private Branch branch;

    private List<Module> modules;
}
