package xperts4all.demo.dto;

import lombok.Data;

@Data
public class UserReq {
    private String userName;
    private String password;
}
