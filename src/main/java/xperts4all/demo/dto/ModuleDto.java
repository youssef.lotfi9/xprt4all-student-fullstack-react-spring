package xperts4all.demo.dto;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ModuleDto {
    private UUID id;

    private String name;

    private int hours;
}
